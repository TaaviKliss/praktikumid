package praktikum8;

import lib.TextIO;

public class Inimene {
	
	String _nimi;
	int _vanus;
	
	public Inimene(String nimi, int vanus) {
		
		this._nimi = nimi;
		this._vanus = vanus;
	}
	
	public void tervita() {
		
		TextIO.putln("Tere, minu nimi on " + _nimi + " ja olen " + _vanus + "-aastane.");
	}
	
	@Override
	public String toString() {
		
		return _nimi + ", " + _vanus;
	}
}
