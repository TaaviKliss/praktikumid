package praktikum8;

import lib.TextIO;

public class SidekriipsugaSona {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Tere tulemast, palun sisestage oma nimi: ");
		String nimi = TextIO.getlnString();
		System.out.println("Tere, " + nimi + ", palun sisestage sõnad, mida tähthaaval välja kirjutada:");
		tryki();
	}

	public static void tryki() {

		String sona = TextIO.getlnString().toUpperCase();

		String uusSona = "";
		for (int i = 0; i < sona.length(); i++) {
			uusSona = sona.charAt(i) + "-";
			System.out.print(uusSona);
		}
	}

}
