package praktikum8;

import lib.TextIO;

public class Palindroom {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Tere tulemast, palun sisestage oma nimi: ");
		String nimi = TextIO.getlnString();
		System.out.println("Tere, " + nimi + ", palun sisestage sõna, mida kontrollida: ");
		String sona = TextIO.getlnString();
		System.out.println(sonaKontroll(sona));
	}
	
	public static boolean sonaKontroll(String a) {
		
		String sonaTagurpidi = "";

		for (int i = 0; i < a.length(); i++) {
			
			sonaTagurpidi = a.charAt(i) + sonaTagurpidi;
		}

		if(a.equals(sonaTagurpidi))
			return true;
		return false;
	}

}
