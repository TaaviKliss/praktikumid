package praktikum8;

import java.util.ArrayList;

import lib.TextIO;

public class Andmebaas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Tere tulemast andmebaasi, palun sisestage oma nimi: ");
		String kasutajaNimi = TextIO.getlnString();
		System.out.println("Tere, " + kasutajaNimi + ", v�ite hakata sisestama inimeste nimesid ja nende vanuseid");
		andmebaas();
	}
	
	public static void andmebaas() {
		
		ArrayList<Inimene> inimesed = new ArrayList<Inimene>();
		inimesed.add(new Inimene(TextIO.getlnString(), TextIO.getlnInt()));
		
		for (Inimene inimene : inimesed) {
			
			System.out.println(inimene);		
			inimene.tervita();
		}
		
	}

}
