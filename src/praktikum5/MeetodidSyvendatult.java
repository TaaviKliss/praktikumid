package praktikum5;

import lib.TextIO;

public class MeetodidSyvendatult {

	public static void main(String[] args) {
		
		System.out.println("Tere tulemast, palun sisestage oma nimi: ");
		String nimi = TextIO.getln();
//		System.out.println("Tere, " + nimi + ", nüüd palun sisestage arv, mille kuupi soovite leida: ");
//		int arv = TextIO.getInt();
//		System.out.println(arv + " kuup on: " + arvuKuup(arv));
//		System.out.println("Tere, " + nimi + ", nüüd sisestage vahemik, mille sisse peaks vastus jääma: ");
//		int vahemikMin = TextIO.getInt();
//		int vahemikMax = TextIO.getInt();
//		System.out.println("Nüüd pakkuge arv, mis võiks jääda sinna vahemikku: ");
//		ennusta(vahemikMin, vahemikMax);
		System.out.println("Tere, " + nimi + ", mängime kulli ja kirja.");
		String arvutiKysib = "Kas kull (1) või kiri (0) ?";
		kullJaKiri(arvutiKysib);
	}
	
	public static int arvuKuup(int a) {
		
		return (int) Math.pow(a, 3);
	}
	
	public static int ennusta(int a, int b) {
		
		int kasutaja = TextIO.getInt();
		while (kasutaja < a || kasutaja > b) {
			
			System.out.println("Ei jää vahemikku, proovi uuesti: ");
			kasutaja = TextIO.getInt();
		}
		
		System.out.println("Õige, jääb vahemikku.");
		return kasutaja;
	}
	
	public static int kullJaKiri(String kysimus) {
		
		while (true) {
			
			System.out.println(kysimus);
			double arvuti = Math.random();
			random(arvuti);
			int kasutaja = TextIO.getInt();
			System.out.println(arvuti);
			
			if (kasutaja != arvuti) {
				
				System.out.println("Vale.");
			}
			
			else
				System.out.println("Õige.");
		}
	}
	
	public static double random(double arvuti) {
		
		if (arvuti < 0.5)
			return arvuti = 0;
		
		else
			return arvuti = 1;
	}

}
