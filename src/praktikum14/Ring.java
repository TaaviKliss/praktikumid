package praktikum14;

public class Ring {

	Punkt keskpunkt;
	double raadius;
	
	public Ring(Punkt minuPunkt, double _raadius) {

		this.keskpunkt = minuPunkt;
		this.raadius = _raadius;
	}
	
	public double ymberm66t() {
		
		double p = 2 * Math.PI * raadius;
		return p;
	}
	
	public double pindala() {
		
		double s = Math.PI * Math.pow(raadius, 2);
		return s;
	}
	
	@Override
	public String toString() {
		

		return "Ringi, keskpunktga " + keskpunkt + " ning raadiusega " + raadius + " ümbermõõt on "
		+ ymberm66t() + " ning pindala " + pindala();
	}

}
