package praktikum14;

public class Punkt {

	double x;
	double y;
	
	public Punkt() {
		
		
	}
	
	public Punkt(double _x, double _y) {
		
		this.x = _x;
		this.y = _y;
	}
	
	@Override
	public String toString() {
		

		return "(x = " + x + ", y = " + y + ")";
	}
}
