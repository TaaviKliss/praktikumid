package praktikum14;

public class Joon {

	Punkt punkt1;
	Punkt punkt2;
	
	public Joon(Punkt minuPunkt, Punkt veelYksPunkt) {
		
		this.punkt1 = minuPunkt;
		this.punkt2 = veelYksPunkt;
	}
	
	public double pikkus() {
		
		double laiuseVahe = punkt2.x - punkt1.x;
		double pikkuseVahe = punkt2.y - punkt1.y;
		double z = Math.sqrt(Math.pow(laiuseVahe, 2) + Math.pow(pikkuseVahe, 2));
		return z;
	}
	
	@Override
	public String toString() {
		

		return "Joone algpunkt on " + punkt1 + " ning lõpp-punkt on " + punkt2;
	}
}
