package praktikum14;

public class Katsetused {

	public static void main(String[] args) {

		Punkt minuPunkt = new Punkt();
		minuPunkt.x = 0;
		minuPunkt.y = 0;
		
		Punkt veelYksPunkt = new Punkt(100, 0);
		System.out.println(veelYksPunkt);
		
		Joon minuJoon = new Joon(minuPunkt, veelYksPunkt);
		System.out.println(minuJoon);
		System.out.println("Joone pikkus on: " + minuJoon.pikkus());
		
		Ring minuRing = new Ring(minuPunkt, 5);
		System.out.println(minuRing);		
	}

}
