package praktikum12;

import lib.TextIO;

public class YhemootmelineTaisarv {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Tere tulemast, palun sisestage oma nimi: ");
		String nimi = TextIO.getlnString();
		System.out.println("Tere, " + nimi + ".");
		// int[] taisArvud = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
		// tryki(taisArvud);
		int[][] taisArvud = { { 1, 2, 3, 4, 7 }, { 6, 7, 8, 12, 5 }, { 1, 2, 15, 4, 5 }, { 1, 9, 3, 4, 5 },
				{ 3, 2, 3, 4, 5 } };

		// ridadeSummad(taisArvud);
		// korvalDiagonaaliSumma(taisArvud);
		trykiMaatriks(taisArvud);
		System.out.println(miinimum(taisArvud));
	}

	public static void tryki(int[] a) {

		for (int i = 0; i < a.length; i++) {

			System.out.print(a[i] + " ");
		}

		System.out.println();
	}

	public static void trykiMaatriks(int[][] m) {

		System.out.println("Teile väljastatakse nüüd arvud maatriksist");
		for (int i = 0; i < m.length; i++) {

			tryki(m[i]);
		}
	}

	public static int reaSumma(int[] r) {

		int summa = 0;
		for (int i : r) {
			summa += i;
		}
		return summa;
	}

	public static int[] ridadeSummad(int[][] m) {

		System.out.println("Teile väljastatakse nüüd iga rea elementide summa: ");
		int[] summad = new int[m.length];
		for (int i = 0; i < m.length; i++) {

			summad[i] = reaSumma(m[i]);
			System.out.println(summad[i]);
		}
		return summad;
	}

	public static int korvalDiagonaaliSumma(int[][] m) {

		int diagonaaliSumma = 0;
		for (int i = m.length - 1; i >= 0; i--) {
			for (int j = m.length - 1; j >= 0; j--) {

				if (i == j) {

					diagonaaliSumma += m[i][j];
					System.out.println(diagonaaliSumma);
				}
			}
		}
		return diagonaaliSumma;
	}

	public static int reaMaksimum(int[] rida) {

		int seniSuurim = rida[0];
		for (int i = 0; i < rida.length; i++) {

			if (rida[i] > seniSuurim) {

				seniSuurim = rida[i];
			}
		}
		return seniSuurim;
	}

	public static int[] ridadeMaksimumid(int[][] m) {

		System.out.println("Väljastan ridade maksimumid: ");
		int[] suurimad = new int[m.length];
		for (int i = 0; i < suurimad.length; i++) {

			suurimad[i] = reaMaksimum(m[i]);
			System.out.println(suurimad[i]);
		}
		return suurimad;
	}

	public static int miinimum(int[][] m) {

		System.out.println("Väljastan väikseima maatriksist: ");
		int seniVaikseim = m[0][0];
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m.length; j++) {

				if (m[i][j] < seniVaikseim) {

					seniVaikseim = m[i][j];
				}

			}
		}
		return seniVaikseim;
	}

}
