package teineKT;

import java.util.Arrays;

public class TeineUlesanne {

	public static void main(String[] args) {
		
		
		System.out.println(score(new int[] { 4, 1, 2, 3, 0 })); // 9
		// Your tests here
	}

	public static int score(int[] points) {
	
		Arrays.sort(points);
		
		int summa = 0;
		
		for (int i = 2; i < points.length; i++) {
			
			summa = summa + points[i];
		}
		
		return summa; // TODO!!! Your program here
	}
}
