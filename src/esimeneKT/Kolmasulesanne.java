package esimeneKT;

public class Kolmasulesanne {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println(ruutudeSumma(new int[] {5}));
	}

	public static int ruutudeSumma(int[] m) {

		double summa;
		summa = 0;
		for (int i = 0; i < m.length; i++) {
			summa = summa + (m[i] * m[i]);
		}
		return (int) summa;
	}

}
