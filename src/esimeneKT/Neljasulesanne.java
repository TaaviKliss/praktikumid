package esimeneKT;

public class Neljasulesanne {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println(allaKeskmise(new double[] { 1.0, 5.0, 15.0, 8.0 }));
	}

	public static int allaKeskmise(double[] d) {

		double koguSumma;
		double keskmine;
		double ariSumma;
		koguSumma = 0;
		ariSumma = 0;

		for (int i = 0; i < d.length; i++) {
			koguSumma = koguSumma + d[i];
		}

		keskmine = koguSumma / d.length;

		for (int i = 0; i < d.length; i++) {
			if (d[i] < keskmine) {

				ariSumma = ariSumma + 1;
			}
		}

		return (int) ariSumma;
	}

}
