package praktikum6;

import lib.TextIO;

public class KullJaKiri {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Tere tulemast mängu, teie algkapital on 100 eurot.");
		int kasutajaKapital = 100;

		while (kasutajaKapital > 0) {
			int panus = teePanus(kasutajaKapital);
			boolean myndivise = viskaMynti();

			if (myndivise == true) {

				kasutajaKapital = kasutajaKapital + (panus * 2);
			}
			
			else {
				
				kasutajaKapital = kasutajaKapital - panus;
			}
			naitaRaha(kasutajaKapital);
		}
		System.out.println("Teil pole enam piisavalt raha, et edasi mängida.");
	}

	public static int teePanus(int a) {

		System.out.println("Sisesta panus (max 25 eurot): ");
		int b = TextIO.getInt();
		a = a - b;
		return b;
	}

	public static int naitaRaha(int a) {

		System.out.println("Nüüd on sul raha " + a + " eurot.");
		return a;
	}

	public static boolean viskaMynti() {

		System.out.println("Pakkuge, kas tuli kull (1) või kiri (0): ");
		double arvutiSisestus = Math.random();

		if (arvutiSisestus < 0.5) {

			arvutiSisestus = 0;
		}

		else {

			arvutiSisestus = 1;
		}

		int kasutajaSisestus = TextIO.getInt();

		if (kasutajaSisestus == arvutiSisestus) {

			System.out.println("Õigesti arvatud!");
			return true;
		}

		else {

			System.out.println("Valesti arvatud!");
			return false;
		}
	}
}
