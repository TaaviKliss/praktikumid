package praktikum6;

import lib.TextIO;

public class AraArvamisMang {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Tere tulemast!");

		int arvutiValis = arvutiArv();
		kasutajaKontroll(arvutiValis);
	}

	public static int arvutiArv() {

		double arvuti = Math.random() * 100;
		System.out.println("Arvuti valis välja ühe arvu vahemikus 1 - 100, proovige arv ära arvata: ");
		return (int) arvuti;
	}

	public static int kasutajaKontroll(int arvuti) {

		int kasutaja = TextIO.getInt();

		while (arvuti != kasutaja) {

			if (arvuti > kasutaja) {

				System.out.println("Vale vastus, arv on suurem");
				kasutaja = TextIO.getInt();
			}

			else if (arvuti < kasutaja) {

				System.out.println("Vale vastus, arv on väiksem");
				kasutaja = TextIO.getInt();
			}
		}
		
		System.out.println("Õige vastus!");
		return arvuti;
	}
}
