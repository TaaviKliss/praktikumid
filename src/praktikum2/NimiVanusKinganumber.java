package praktikum2;

import lib.TextIO;

public class NimiVanusKinganumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Tere tulemast, palun sisestage oma nimi: ");
		String nimi = TextIO.getlnString();
//		System.out.println("Tere, " + nimi + ", nüüd sisestage oma vanus");
//		int vanus = TextIO.getlnInt();
//		System.out.println("Te olete " + vanus + " aastat vana. Mis on teie kinganumber: ");
//		int kinganumber = TextIO.getlnInt();
//		System.out.println("Teie kinganumber on " + kinganumber + ".");
//		System.out.println("Tere, " + nimi + ", sisestage esimene tegur: ");
//		int esimeneTegur = TextIO.getlnInt();
//		System.out.println("Nüüd sisestage teine tegur: ");
//		int teineTegur = TextIO.getlnInt();
//		System.out.println("Vastus on: " + korruta(esimeneTegur, teineTegur));
//		System.out.println("Tere, " + nimi + ", sisestage inimeste arv: ");
//		int inimesteArv = TextIO.getlnInt();
//		System.out.println("Nüüd siestage grupi suurus: ");
//		int grupiSuurus = TextIO.getInt();
//		System.out.println("Tekitada saab " + jagaJaagita(inimesteArv, grupiSuurus) + " ning üle jääb " + leiaJaak(inimesteArv, grupiSuurus) + " inimest.");
//		int nimePikkus = nimi.length();
//		System.out.println("Teie nimes on " + nimePikkus + " tähte.");
		System.out.println("Tere, " + nimi + ", sisestage sõna, mille tähti soovite peita: ");
		String sona = TextIO.getlnString().replace('a', '_');
		System.out.println("Sõna näeb nüüd välja selline: " + sona);

	}
	
	public static int korruta(int a, int b) {
		
		return a * b;
	}
	
	public static int jagaJaagita(int a, int b) {
		
		return a / b;
	}
	
	public static int leiaJaak(int a, int b) {
		
		return a % b;
	}

}
