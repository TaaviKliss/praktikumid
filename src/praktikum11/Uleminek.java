package praktikum11;

import java.applet.*;
import java.awt.Color;
import java.awt.Graphics;

public class Uleminek extends Applet {

	@Override
	public void paint(Graphics g) {

		int w = getWidth();
		int h = getHeight();

		double varviMuutus = 255. / h;
		
		for (int i = 0; i < h; i++) {
			
			int varvikood = (int) (255 - i * varviMuutus);
			Color minuVarv = new Color(varvikood, varvikood, varvikood);
			g.setColor(minuVarv);
			g.drawLine(0, i, w, i);
		}
	}

}
