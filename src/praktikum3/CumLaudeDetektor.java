package praktikum3;

import lib.TextIO;

public class CumLaudeDetektor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Tere, palun, sisestage oma nimi: ");
		String nimi = TextIO.getln();
//		System.out.println("Tere, " + nimi + ", sisestage oma keskmine hinne: ");
//	    double keskmineHinne = TextIO.getDouble();
//    	System.out.println("Nüüd sisestage oma lõputöö hinne: ");
//		int lõputööHinne = TextIO.getInt();
//		System.out.println(kontroll(keskmineHinne, lõputööHinne));
//		System.out.println("Tere, " + nimi + ", sisestage esimene vanus: ");
//		int esimeneVanus = TextIO.getInt();
//		System.out.println("Nüüd sisestage teine vanus: ");
//		int teineVanus = TextIO.getInt();
//		int vanuseVahe = Math.abs(esimeneVanus-teineVanus);
//		System.out.println(vanuseKontroll(vanuseVahe));
		System.out.println("Tere, " + nimi + ", valige endale parool: ");
		String esimeneParool = TextIO.getlnString();
		System.out.println("Nüüd proovige sisestada see parool õigesti: ");
		String teineParool = TextIO.getlnString();
		System.out.println(parooliKontroll(esimeneParool, teineParool));
	}
	
	public static boolean kontroll (double a, int b) {
		
		if (a > 5 || a < 2 || b > 5 || b < 2) {
			
			System.out.println("Vigane sisestus!");
			return false;
		}
		
		else if (a > 4.5 && b == 5) {
			
			System.out.println("Jah, saad Cum Laude diplomile.");
			return true;
		}
		
		else {
			
			System.out.println("Ei saa cum Laude diplomile");
			return false;
		}
	}
	
	public static boolean vanuseKontroll(int a) {
		
		if (a <= 5) {
			
			System.out.println("Sobib!");
			return true;
		}
		
		else if (a > 10) {
			
			System.out.println("Täiesti lootusetu!");
			return false;
		}
		
		else {
			
			System.out.println("Veits jama!");
			return false;
		}
	}
	
	public static boolean parooliKontroll(String a, String b) {
		
		if (a.equals(b)) {
			System.out.println("Õige parool!");
			return true;
			
		}
		else {
			
			System.out.println("Vale parool!");
			return false;
		}
	}

}
