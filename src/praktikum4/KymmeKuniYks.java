package praktikum4;

import lib.TextIO;

public class KymmeKuniYks {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Tere, palun sisestage oma nimi: ");
		String nimi = TextIO.getln();
//		System.out.println("Tere, " + nimi + ", väljastan numbrid ühest kümneni: ");
//		int esimeneNumber = 1;
//		System.out.println(yhestKymneni(esimeneNumber));
//		System.out.println("Tere, " + nimi + ", väljastan paarisarvud ühest kümneni: ");
//		int esimeneNumber = 0;
//		System.out.println(paarisArvud(esimeneNumber));
//		System.out.println("Tere, " + nimi + ", väljastan kolmega jaguvad arvud 30'st 0'ni: ");
//		int esimeneArv = 30;
//		System.out.println(kolmegaJaguvadArvud(esimeneArv));
//		System.out.println("Tere, " + nimi + ", väljastan ühe diagonaaliga ruudustiku: ");
//		int veeruNumber = 0;
//		int reaNumber = 0;
//		System.out.println(diagonaaligaRuudustik(veeruNumber, reaNumber));
		System.out.println("Tere, " + nimi + ", väljastan kahe diagonaaliga ruudustiku: ");
		int veeruNumber = 0;
		int reaNumber = 0;
		System.out.println(raamistik(veeruNumber) + kaheDiagonaaligaRuudustik(veeruNumber, reaNumber) + raamistik(veeruNumber));
	}

	public static int yhestKymneni(int a) {

		while (a < 10) {

			System.out.print(a + " ");
			a = a + 1;
		}

		return a;
	}

	public static int paarisArvud(int a) {

		while (a < 10) {

			System.out.print(a + " ");
			a = a + 2;
		}

		return a;
	}

	public static int kolmegaJaguvadArvud(int a) {

		while (a > 0) {

			System.out.print(a + " ");
			a = a - 3;
		}

		return a;
	}

	public static String diagonaaligaRuudustik(int a, int b) {

		for (a = 0; a < 7; a++) {

			for (b = 0; b < 7; b++) {

				if (a == b) {

					System.out.print("1 ");
				}
				
				else

					System.out.print("0 ");
			}
			
			System.out.println();

		}
		return ("Valmis.");
	}
	
	public static String kaheDiagonaaligaRuudustik(int a, int b) {

		for (a = 0; a < 7; a++) {
			
			for (b = 0; b < 7; b++) {
				
				if (a == b) {
					
					System.out.print("x ");
				}
				
				else
					
					System.out.print("0 ");
			}
			
			System.out.println();
		}
		
		return ("Valmis.");
	}
	
	public static String raamistik(int a) {
		
		
		for (a = 0; a < 13; a++) {
			
			System.out.print("-");
		}
		
		System.out.println();
		
		
		return ("");
	}

}
