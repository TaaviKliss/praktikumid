package praktikum13;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;

public class Faililugeja {
	
	public static ArrayList<String> loeFail(String failinimi) {
		
	    // otsime samast kataloogist kala.txt-nimelist faili
		File file = new File(failinimi.replaceAll("%20", " "));
		ArrayList<String> read = new ArrayList<String>();
		
		try {
		    // avame faili lugemise jaoks
			BufferedReader in = new BufferedReader(new FileReader(file));
			String rida;

			// loeme failist rida haaval
			while ((rida = in.readLine()) != null) {
				read.add(rida);
			}

		}
		catch (FileNotFoundException e) {
		    System.out.println("Faili ei leitud: \n" + e.getMessage());
		}
		catch (Exception e) {
			System.out.println("Error, jee, mingi muu error: " + e.getMessage());
		}
		
		return read;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	    
		// punkt tähistab jooksvat kataloogi
	    String kataloogitee = Faililugeja.class.getResource(".").getPath();
	    ArrayList<String> failiSisu = loeFail(kataloogitee + "kala.txt");
	    System.out.println(failiSisu);
	    Collections.sort(failiSisu);
	    System.out.println(failiSisu);
	}

}
