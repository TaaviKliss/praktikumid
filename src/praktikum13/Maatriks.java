package praktikum13;

import java.util.ArrayList;

public class Maatriks {
	
	public static ArrayList<ArrayList<Double>> transponeeri(ArrayList<ArrayList<Double>> sisendMaatriks) {
		
		return sisendMaatriks;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// punkt tähistab jooksvat kataloogi
		String kataloogitee = Maatriks.class.getResource(".").getPath();
		ArrayList<String> failiSisu = Faililugeja.loeFail(kataloogitee + "maatriks.txt");
		System.out.println(failiSisu);
		
		ArrayList<ArrayList<Double>> maatriks = new ArrayList<ArrayList<Double>>();
		
		for (String rida : failiSisu) {
			
			String[] elemendid = rida.split(" ");
			ArrayList<Double> maatriksiRida = new ArrayList<Double>();
			for (String el : elemendid) {
				
				double nr = Double.parseDouble(el);
				maatriksiRida.add(nr);
			}
			
			maatriks.add(maatriksiRida);
		}
		
		System.out.println(maatriks);
	}

}
