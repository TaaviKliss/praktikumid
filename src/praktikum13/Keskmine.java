package praktikum13;

import java.util.ArrayList;

public class Keskmine {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// punkt tähistab jooksvat kataloogi
		String kataloogitee = Keskmine.class.getResource(".").getPath();
		ArrayList<String> failiSisu = Faililugeja.loeFail(kataloogitee + "numbrid.txt");
		System.out.println(failiSisu);
		double summa = 0;
		int vigaseidRidu = 0;
		
		for (String rida : failiSisu) {
			
			try {
				double nr = Double.parseDouble(rida);
				summa += nr;
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				System.out.println("See ei ole number: " + rida);
				vigaseidRidu++;
			}
		}
		
		System.out.println("Summa on " + summa);
		
		double keskmine = summa / (failiSisu.size() - vigaseidRidu);
		
		System.out.println("Aritmeetiline keskmine on: " + keskmine);
	}
}
