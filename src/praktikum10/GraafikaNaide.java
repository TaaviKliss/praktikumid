package praktikum10;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class GraafikaNaide extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("JavaFX-iga joonistamise näide");
		Group root = new Group();
		Canvas canvas = new Canvas(400, 400);
		GraphicsContext gc = canvas.getGraphicsContext2D();
		joonista(gc);
		root.getChildren().add(canvas);
		primaryStage.setScene(new Scene(root));
		primaryStage.show();
	}

	private void joonista(GraphicsContext gc) {
		gc.setFill(Color.BLACK);
		gc.setStroke(Color.RED);
		gc.setLineWidth(15);
		gc.fillRoundRect(110, 1, 150, 150, 40, 40);
		gc.strokeLine(180, 150, 180, 250);
		gc.strokeLine(180, 170, 50, 180);
		gc.strokeLine(180, 170, 300, 180);
		gc.strokeLine(180, 250, 300, 300);
		gc.strokeLine(180, 250, 50, 300);
		gc.strokeOval(125, 10, 50, 50);
		gc.strokeOval(200, 10, 50, 50);
		gc.setFill(Color.RED);
		gc.fillRoundRect(160, 80, 50, 50, 10, 10);
		gc.fillText("Metalit suppordid?", 100, 370);
	}
}
