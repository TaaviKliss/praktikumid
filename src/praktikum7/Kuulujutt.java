package praktikum7;

import lib.TextIO;

public class Kuulujutt {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Tere tulemast jututuppa! Palun sisestage oma nimi: ");
		String nimi = TextIO.getln();
		System.out.println("Tere, " + nimi + ", kas sa teadsid, et...");
		String[] mehed = {"Taavi", "Claus", "Peep", "Gen", "Risto"};
		String[] naised = {"Liinat", "Karmelyt", "Laurat", "Hendrikat", "Teelet"};
		String[] tegusõnad = {"sebis", "pettis", "kallistas"};
		System.out.println(kuulujutuGeneraator(mehed, naised, tegusõnad));
	}
	
	public static String kuulujutuGeneraator (String[] mehed, String[] naised, String[] tegusõnad) {
		
		int suvalineMees = (int)(Math.random() * mehed.length);
		int suvalineNaine = (int)(Math.random() * naised.length);
		int suvalineTegu = (int)(Math.random() * tegusõnad.length);
		return mehed[suvalineMees] + " " + tegusõnad[suvalineTegu]+ " " + naised[suvalineNaine] + ".";
		
	}

}
