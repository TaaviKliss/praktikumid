package praktikum7;

import lib.TextIO;

public class KymmeArvuMassiiv {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Tere tulamst! Palun sisestage oma nimi: ");
		String nimi = TextIO.getln();
		System.out.println("Tere, " + nimi + ", palun sisestage 10 arvu, mida Teile vastupidises järjekorras kuvada");
		arvuMassiiv();
	}

	public static void arvuMassiiv() {

		int[] arvud = new int[10];
		for (int i = 0; i < arvud.length; i++) {

			arvud[i] = TextIO.getlnInt();
		}
		for (int i = 9; i >= 0; i--) {

			System.out.print(arvud[i] + " ");
		}

	}

}
